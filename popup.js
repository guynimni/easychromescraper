let myTab = null;

document.addEventListener('DOMContentLoaded', function() {
	setMyTabByURL("easy", null);

	appendScrapedURLs();

	const scrapeButton = document.getElementById('scrapeResultsButton');
	const clearScrape = document.getElementById('clearUrlsButton');
	const startScrapeBtn = document.getElementById('startScrape');
	const stopScrapeBtn = document.getElementById('stopScrape');
	const clearData = document.getElementById('clearScrape');

	setJSONForDownload();

	clearData.addEventListener('click', function() {
	  clearScrapedData();
	});

	startScrapeBtn.addEventListener('click', function() {
	  storeKey("scraping", true, () => {
	    sendMessageToMyTab('startDataScrape');
    });
	});

	stopScrapeBtn.addEventListener('click', function() {
	  storeKey("scraping", false, () => {
	    sendMessageToMyTab('stopDataScrape');
    });
	});

	clearScrape.addEventListener('click', function() {
	  storeKey("urls", [], () => {
	    appendScrapedURLs();
	    setJSONForDownload();

    });
		sendMessageToMyTab('clearScrapeResults');
	});

	scrapeButton.addEventListener('click', function() {
		sendMessageToMyTab('scrapeResults');
	});
});

function setMyTabByURL(urlString, callback) {
  const queryInfo = {
    currentWindow: true
  };

  chrome.tabs.query(queryInfo, function(tabs) {
	  let tab = null;
	  
	  for (var i = 0; i < tabs.length; i++) {
		  if (tabs[i].url.indexOf(urlString) > -1) { // Get the first tab with youtube in it
			  tab = tabs[i];
			  break;
		  }
	  }

	myTab = tab;
  });
}

function sendMessageToMyTab(type, data) {
	if (myTab) {
		chrome.tabs.sendMessage(myTab.id, {type}, null, null); // Send message to the selected tab
	}
	else {
		console.log("NO SELECTED TAB");
	}
}

function appendScrapedURLs(urls) {

  retrieveKey("urls", (data) => {
    data = urls || data || [];
    const dataHeader = document.querySelector('#data h3');
    dataHeader.innerText = `Scraped URL's list (${data.length})`;

    const urlsNode = document.getElementById("urls");

    while (urlsNode.firstChild) {
      urlsNode.removeChild(urlsNode.firstChild);
    }

    data.forEach(url => {
      const node = document.createElement("DIV");
      const textnode = document.createTextNode(url);
      node.appendChild(textnode);
      urlsNode.appendChild(node);
    });
  });
}

function setJSONForDownload() {
  retrieveKey("scraped", (obj) => {
    obj = obj || [];
    const length = obj.length;
    if (length) {
      const str = JSON.stringify(obj);
      console.log(str);
      const data = new TextEncoder().encode(str);

      const blob = new Blob( [ data ], {
        type: "application/json;charset=utf-8"
      });

      const url = URL.createObjectURL( blob );
      const downloadElem = document.getElementById('download');
      downloadElem.innerText = `Download ${length} pages scraped`;
      downloadElem.setAttribute( 'href', url );
      downloadElem.setAttribute( 'download', 'data.json' );
    }
    else {
      document.getElementById('download').innerText = `No data to download...`;
    }
  });
}

function storeKey(key, value, callback) {
  chrome.storage.local.set({[key]: value}, function() {
    callback(value);
  });
}

function retrieveKey(key, callback) {
  chrome.storage.local.get([`${key}`], function(result) {
    callback(result[key]);
  });
}

function clearScrapedData() {
  storeKey("scraped", [], () => {
  });
}

chrome.runtime.onMessage.addListener(
  function(request, sender, sendResponse) {
    switch (request.type) {
      case "INFORM_LAST_PAGE": {
        setJSONForDownload();
        break;
      }
      case "SET_URLS_TO_SCRAPE": {
        appendScrapedURLs(request.data);
        break;
      }
    }
  }
);
