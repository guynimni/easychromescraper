function storeKey(key, value, callback) {
  chrome.storage.local.set({[key]: value}, function() {
    callback(value);
  });
}

function retrieveKey(key, callback) {
  chrome.storage.local.get([`${key}`], function(result) {
    callback(result[key]);
  });
}

chrome.runtime.onMessage.addListener(
  function(request, sender, sendResponse) {
    switch (request.type) {
      case "INFORM_LAST_PAGE": {
        storeKey("scraping", false, () => {
          sendResponse({status: "OK"});
        });
        break;
      }
      case "ADD_SCRAPED_DATA": {
        retrieveKey("scraped", (scraped) => {
          scraped = scraped || [];
          const alreadyExist = scraped.find(singleDataItem => singleDataItem.phone == request.data.phone && singleDataItem.name == request.data.name);
          if (alreadyExist)
            sendResponse({status: "OK"});
          else {
            scraped.push(request.data);
            storeKey("scraped", scraped, (urls) => {
              sendResponse({status: "OK"});
            });
          }
        });
        break;
      }
      case "SET_URLS_TO_SCRAPE": {
        storeKey("urls", request.data, (urls) => {
          sendResponse({status: "OK"});
        });
        break;
      }
      case "GET_INITIAL_DATA": {
        retrieveKey("urls", (urls) => {
          retrieveKey("scraping", (scraping) => {
            sendResponse({urls, scraping});
          });
        });
        break;
      }
    }
    return true;
  }
);
