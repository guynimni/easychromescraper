window.gnscraper = {
  scraping: false,
  urls: []
};

chrome.runtime.onMessage.addListener(function(message, sender) { // Listen to messages from the popup.js
	const func = window[message.type];
	
	if (func) {
		func();
	}
});

function scrapeResults() {
  console.log("Scraping results");

  const currentResults = (window.gnscraper.urls || []).concat($("a[href^='/page']").toArray().map(elem => elem.href));

  updateUrlsList(currentResults);
}

function updateUrlsList(urls, callback) {
  chrome.runtime.sendMessage({type: "SET_URLS_TO_SCRAPE", data: urls}, function(response) {
    window.gnscraper.urls = urls;
    console.log(response.status);
    callback();
  });
}

function clearScrapeResults() {
  localStorage.setItem("urls", JSON.stringify([]));
}

function startDataScrape() {
  window.gnscraper.scraping = true;
  scrapingRoutine();
}

function stopDataScrape() {
  window.gnscraper.scraping = false;
}

function scrapeCurrentPage() {
  const pageData = {};
  pageData.phone = $("span.biz-phone-number").text();
  pageData.address = $("span.title-address-text").text();
  pageData.name = $("h1#title").text();
  pageData.facebook = "";
  pageData.instagram = "";
  pageData.url = "";
  $("a.biz-action-ext-link").toArray().forEach(elem => {
    const link = $(elem).attr("href");
    if (link.includes("facebook"))
      pageData.facebook = link;
    else if (link.includes("instagram"))
      pageData.instagram = link;
    else {
      pageData.url = link;
    }
  });

  return pageData;
}

function informLastPage(callback) {
  chrome.runtime.sendMessage({type: "INFORM_LAST_PAGE"}, function(response) {
    if (callback)
      callback();
  });
}

function sendScrapedData(data, callback) {
  chrome.runtime.sendMessage({type: "ADD_SCRAPED_DATA", data}, function(response) {
    callback();
  });
}

function getRandomSeconds() {
  const max = 15;
  const min = 2;
  const seconds = Math.random() * (max - min) + min;
  console.log(`Sleep for ${seconds} seconds`);
  return seconds;
}

function scrapingRoutine() {
  if (window.gnscraper.scraping) {
    if (!window.gnscraper.urls.length) {
      informLastPage();
    }
    else {
      const currentURL = document.location.href;
      const currentURLIndex = window.gnscraper.urls.indexOf(currentURL);
      let pageData = {};
      if (currentURLIndex > -1) {
        pageData = scrapeCurrentPage();
        sendScrapedData(pageData, () => {
          window.gnscraper.urls.splice(currentURLIndex, 1);
          updateUrlsList(window.gnscraper.urls, () => {
            if (window.gnscraper.urls[0]) {
              setTimeout(() => {
                document.location = window.gnscraper.urls[0];
              }, getRandomSeconds() * 1000);
            }
            else
              informLastPage();
          });
        });
      }
      else {
        if (window.gnscraper.urls[0]) {
          setTimeout(() => {
            document.location = window.gnscraper.urls[0];
          }, getRandomSeconds() * 1000);
        }
      }
    }
  }
}

(function init() {
  chrome.runtime.sendMessage({type: "GET_INITIAL_DATA"}, function(response) {
    if (response.urls) {
      console.log("Set initial data");
      console.log(response.urls);
      window.gnscraper.urls = response.urls;
      window.gnscraper.scraping = response.scraping;
      console.log(window.gnscraper.scraping ? "Scraping" : "Not scraping");
      scrapingRoutine();
    }
    else {
      console.log("Set initial data to empty array");
      clearScrapeResults()
    }
  });
})();